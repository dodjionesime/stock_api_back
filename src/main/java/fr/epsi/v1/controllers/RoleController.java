package fr.epsi.v1.controllers;

import fr.epsi.models.Role;
import fr.epsi.services.RoleService;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/roles")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RoleController {
    @Inject
    RoleService roleService;

    @GET
    public List<Role> getRoles() {
        return roleService.getAllRoles();
    }

    @POST
    public Role addNewRole(@RequestBody Role role) {
        return roleService.addRole(role);
    }

}

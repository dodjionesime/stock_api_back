package fr.epsi.v1.controllers;

import fr.epsi.models.Fournisseur;
import fr.epsi.services.FournisseurService;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/fournisseurs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FournisseurController {
    @Inject
    FournisseurService fournisseurService;

    @GET
    public List<Fournisseur> getFournisseurs() {
        return fournisseurService.getAllFournisseur();
    }

    @POST
    public Fournisseur addNewFournisseur(@RequestBody Fournisseur fournisseur) {
        return fournisseurService.addFournisseur(fournisseur);
    }

    @PUT
    public Fournisseur updateFournisseur(@RequestBody Fournisseur fournisseur){
        return  fournisseurService.updateFournisseur(fournisseur);
    }

    @DELETE
    public void deleteFournisseur(@RequestBody Fournisseur fournisseur){
        fournisseurService.deleteFournisseur(fournisseur);
    }

}

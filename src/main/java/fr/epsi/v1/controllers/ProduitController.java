package fr.epsi.v1.controllers;

import fr.epsi.models.Produit;
import fr.epsi.services.ProduitService;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/produits")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProduitController {
    @Inject
    ProduitService produitService;

    @GET
    public List<Produit> getCategories() {
        return produitService.getAllProducts();
    }

    @POST
    public Produit addNewProduit(@RequestBody Produit product) {
        return produitService.addProduct(product);
    }


    @PUT
    public Produit updateProduit(@RequestBody Produit product) {
        return produitService.updateProduct(product);
    }

    @DELETE
    public void deleteProduit(@RequestBody Produit product) {
        produitService.deleteProduct(product);
    }


}

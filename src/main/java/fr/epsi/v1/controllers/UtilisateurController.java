package fr.epsi.v1.controllers;

import fr.epsi.models.Utilisateur;
import fr.epsi.services.UtilisateurService;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/utilisateurs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UtilisateurController {
    @Inject
    UtilisateurService utilisateurService;

    @GET
    public List<Utilisateur> getUsers() {
        return utilisateurService.getAllUsers();
    }

    @POST
    @Path("/get_user")
    public Utilisateur getUser(@RequestBody Utilisateur utilisateur) {
        return utilisateurService.getUtilisateur(utilisateur);
    }

    @POST
    public Utilisateur addNewUser(@RequestBody Utilisateur utilisateur) {
        return utilisateurService.addUtilisateur(utilisateur);
    }

    @PUT
    public Utilisateur updateUser(@RequestBody Utilisateur utilisateur) {
        return utilisateurService.updateUtilisateur(utilisateur);
    }

    @DELETE
    public void deleteUser(@RequestBody Utilisateur utilisateur) {
        utilisateurService.deleteUtilisateur(utilisateur);
    }
}

package fr.epsi.v1.controllers;

import fr.epsi.models.Categorie;
import fr.epsi.services.CategorieService;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategorieController {
    @Inject
    CategorieService categorieService;

    @GET
    public List<Categorie> getCategories() {
        return categorieService.getAllCategories();
    }

    @POST
    public Categorie addNewCategorie(@RequestBody Categorie categorie) {
        return categorieService.addCategorie(categorie);
    }
    @PUT
    public Categorie updateCategorie(@RequestBody Categorie categorie){
        return categorieService.updateCategorie(categorie);
    }

    @DELETE
    public  void deleteCategorie(@RequestBody Categorie categorie){
        categorieService.deleteCategorie(categorie);
    }


}

package fr.epsi.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.*;
import java.util.List;

@Entity()
public class Fournisseur extends PanacheEntity {

    @Column(name = "denomination")
    private String denomination;

    @Column(name = "siret")
    private String siret;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "nom_gerant")
    private String nomGerant;

    @Column(name = "prenom_gerant")
    private String prenomGerant;

    @OneToMany(mappedBy = "fournisseur")
    private List<Produit> produitsFournisseur;

    public Fournisseur() {
    }

    public Fournisseur(String denomination, String siret, String adresse, String nomGerant, String prenomGerant) {
        this.denomination = denomination;
        this.siret = siret;
        this.adresse = adresse;
        this.nomGerant = nomGerant;
        this.prenomGerant = prenomGerant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNomGerant() {
        return nomGerant;
    }

    public void setNomGerant(String nomGerant) {
        this.nomGerant = nomGerant;
    }

    public String getPrenomGerant() {
        return prenomGerant;
    }

    public void setPrenomGerant(String prenomGerant) {
        this.prenomGerant = prenomGerant;
    }

}

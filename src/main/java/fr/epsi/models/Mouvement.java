package fr.epsi.models;


import fr.epsi.Statuts;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity()
public class Mouvement extends PanacheEntity {

    @Column(name = "dateMouvement")
    private Date date;

    @ManyToOne
    @JsonbTransient
    private Produit produitsMouvement;

    @Column(name = "statut")
    @Enumerated(EnumType.STRING)
    private Statuts statuts;

    public Long getId() {
        return id;
    }

    public Mouvement() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Produit getProduitsMouvement() {
        return produitsMouvement;
    }

    public void setProduitsMouvement(Produit produitsMouvement) {
        this.produitsMouvement = produitsMouvement;
    }

    public Statuts getStatuts() {
        return statuts;
    }

    public void setStatuts(Statuts statuts) {
        this.statuts = statuts;
    }
}

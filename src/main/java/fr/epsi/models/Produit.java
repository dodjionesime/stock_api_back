package fr.epsi.models;

import fr.epsi.Statuts;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.List;

@Entity()
public class Produit extends PanacheEntity {
    @Column(name = "description")
    private String description;

    @Column(name = "statut")
    @Enumerated(EnumType.STRING)
    private Statuts statuts;

    @Column(name = "nom")
    private String nom;

    @Column(name = "quantite")
    private Integer quantite;

    @ManyToOne
    private Categorie categorie;

    @ManyToOne
    private Fournisseur fournisseur;

    @OneToMany(mappedBy = "produit")

    private List<ImageProduit> imageProduits;

    @OneToMany(mappedBy = "produitsMouvement")

    private List<Mouvement> produitsMouvement;

    public Produit() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Statuts getStatuts() {
        return statuts;
    }

    public void setStatuts(Statuts statuts) {
        this.statuts = statuts;
    }

    public List<Mouvement> getProduitsMouvement() {
        return produitsMouvement;
    }

    public void setProduitsMouvement(List<Mouvement> produitMouvement) {
        this.produitsMouvement = produitMouvement;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public List<ImageProduit> getImageProduits() {
        return imageProduits;
    }

    public void setImageProduits(List<ImageProduit> imageProduits) {
        this.imageProduits = imageProduits;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

}

package fr.epsi.models;


import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.List;

@Entity()
public class Role extends PanacheEntity {
    @Column(name = "libelle")
    private String libelle;

    @OneToMany(mappedBy = "role")
    @JsonbTransient
    private List<Utilisateur> utilisateurs;

    public Role(String libelle) {
        this.libelle = libelle;
    }

    public Role() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public List<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(List<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

}

package fr.epsi.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.List;

@Entity()
public class Categorie extends PanacheEntity {

    @Column(name = "libelle")
    private String libelle;

    @OneToMany(mappedBy = "categorie")
    @JsonbTransient
    private List<Produit> produit;

    public Categorie(Long idCategorie, String libelleCategorie) {
        this.id = idCategorie;
        this.libelle = libelleCategorie;
    }

    public Categorie() {
    }

    public Categorie(String libelle) {
        this.libelle = libelle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public List<Produit> getProduit() {
        return produit;
    }

    public void setProduit(List<Produit> produit) {
        this.produit = produit;
    }


}

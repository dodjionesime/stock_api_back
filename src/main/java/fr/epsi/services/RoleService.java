package fr.epsi.services;

import fr.epsi.models.Role;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;

@Singleton
public class RoleService {

    public List<Role> getAllRoles() {
        //Méthode permettant de récupérer tous les rôles dans la base de données fournit par panacheEntity
        return Role.findAll().list();
    }

    //Méthode permettant de sauvegarder directement un Rôle dans la base de données fournit par panacheEntity
    @Transactional
    public Role addRole(Role role) {
        if (Role.find("libelle", role.getLibelle()).count() == 0) {
            Role newRole = new Role(role.getLibelle());
            newRole.persist();
            return newRole;
        } else {
            return null;
        }
    }
}

package fr.epsi.services;

import fr.epsi.models.ImageProduit;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ImageService {
    public List<ImageProduit> getAllRoles() {
        //Méthode permettant de récupérer tous les rôles dans la base de données fournit par panacheEntity
        return ImageProduit.findAll().list();
    }
}

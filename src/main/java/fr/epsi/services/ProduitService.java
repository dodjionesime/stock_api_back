package fr.epsi.services;

import fr.epsi.Statuts;
import fr.epsi.models.*;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Singleton
public class ProduitService {

    //Méthode permettant de récupérer tous les produits  dans la base de données fournit par panacheEntity
    public List<Produit> getAllProducts() {
        return Produit.findAll().list();
    }

    //Méthode permettant de sauvegarder directement une Catégorie dans la base de données fournit par panacheEntity
    @Transactional
    public Produit addProduct(Produit produit) {
        if (Produit.find("nom", produit.getNom()).count() == 0) {
            Produit newProduct = new Produit();
/***
 * On ajoute le produit avec ses images sans l'ajouter au mouvement parce qu'il faut ajouter le produit avant d'enregistrer son mouvement
 */
            newProduct.setCategorie(Categorie.find("libelle", produit.getCategorie().getLibelle()).singleResult());
            newProduct.setDescription(produit.getDescription());
            newProduct.setFournisseur(Fournisseur.find("nomGerant", produit.getFournisseur().getNomGerant()).singleResult());
            newProduct.setStatuts(Statuts.CREATED);
            newProduct.setNom(produit.getNom());
            produit.getImageProduits().stream().forEach(
                    imageProduit -> {
                        ImageProduit image = new ImageProduit();
                        image.setLien(imageProduit.getLien());
                        image.setProduit(Produit.find("nom", imageProduit.getProduit().getNom()).singleResult());
                        image.persist();
                        newProduct.getImageProduits().add(image);
                    }
            );
            newProduct.persist();
/***
 * On crée le mouvement et on récupère le produit qui a été persisté dans la bd pour mettre à jour sa liste de mouvement et on
 * update le tout dans la bd
 */
            Mouvement firstMouvement = new Mouvement();
            firstMouvement.setProduitsMouvement(newProduct);
            firstMouvement.setStatuts(newProduct.getStatuts());
            firstMouvement.setDate(new Date());
            firstMouvement.persist();
            Produit retrievedProduit = Produit.find("nom", newProduct.getNom()).singleResult();
            retrievedProduit.getProduitsMouvement().add(firstMouvement);
            retrievedProduit.persist();

            return newProduct;
        } else {
            return null;
        }
    }

    //Une première ébauche pour être sur tout fonctionne à merveille sans la partie des images

    @Transactional
    public Produit updateProduct(Produit produit) {
        Produit produitToUpdate = Produit.find("id", produit.getId()).singleResult();
        produitToUpdate.setDescription(produit.getDescription());
        produitToUpdate.setNom(produit.getNom());
        produitToUpdate.setFournisseur(Fournisseur.find("nomGerant", produit.getFournisseur().getNomGerant()).singleResult());
        produitToUpdate.setStatuts(produit.getStatuts());
        produitToUpdate.persist();

        Mouvement updateMouvement = new Mouvement();
        updateMouvement.setProduitsMouvement(produitToUpdate);
        updateMouvement.setStatuts(produitToUpdate.getStatuts());
        updateMouvement.setDate(new Date());
        updateMouvement.persist();
        Produit retrievedProduit = Produit.find("nom", produitToUpdate.getNom()).singleResult();
        retrievedProduit.getProduitsMouvement().add(updateMouvement);
        retrievedProduit.persist();
        return retrievedProduit;
    }

    @Transactional
    public void deleteProduct(Produit produit){
        Produit produitToDelete = Produit.find("id", produit.getId()).singleResult();
        produitToDelete.getImageProduits().stream().forEach(imageProduit -> imageProduit.delete());
        produitToDelete.getProduitsMouvement().stream().forEach(mouvement -> mouvement.delete());
        produitToDelete.delete();
    }


}


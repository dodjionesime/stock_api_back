package fr.epsi.services;

import fr.epsi.models.Categorie;
import fr.epsi.models.Produit;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;

@Singleton
public class CategorieService {
    // private static final Logger log = LogManager.getLogger(CategorieService.class);

    public List<Categorie> getAllCategories() {
        //Méthode permettant de récupérer toutes les categories dans la base de données fournit par panacheEntity
        return Categorie.findAll().list();
    }

    //Méthode permettant de sauvegarder directement une Catégorie dans la base de données fournit par panacheEntity
    @Transactional
    public Categorie addCategorie(Categorie categorie) {
        if (Categorie.find("libelle", categorie.getLibelle()).count() == 0) {
            Categorie newCategorie = new Categorie(categorie.getLibelle());
            newCategorie.persist();
            return newCategorie;
        } else {
            return null;
        }
    }

    @Transactional
    public Categorie updateCategorie(Categorie categorie){
        Categorie categorieToUpdate = Categorie.find("id",categorie.getId()).singleResult();
        categorieToUpdate.setLibelle(categorie.getLibelle());
        categorieToUpdate.persist();
        return  categorieToUpdate;
    }

    @Transactional
    public void deleteCategorie(Categorie categorie){
        Categorie  categorieToDelete = Categorie.find("id",categorie.getId()).singleResult();
        if(categorieToDelete.isPersistent()){
            categorieToDelete.delete();
        }

    }

}

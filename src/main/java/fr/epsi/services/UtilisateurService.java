package fr.epsi.services;

import fr.epsi.models.Role;
import fr.epsi.models.Utilisateur;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

@Singleton
public class UtilisateurService {
    //Méthode permettant de récupérer tous les utilisateurs dans la base de données fournit par panacheEntity
    public List<Utilisateur> getAllUsers() {
        return Utilisateur.findAll().list();
    }

    @Transactional
    public Utilisateur addUtilisateur(Utilisateur utilisateur) {
        if (Utilisateur.find("mail", utilisateur.getMail()).count() == 0) {
            Utilisateur nouvelUtilisateur = new Utilisateur();
            nouvelUtilisateur.setNom(utilisateur.getNom());
            nouvelUtilisateur.setPrenom(utilisateur.getPrenom());
            nouvelUtilisateur.setMail(utilisateur.getMail());
            Role role = new Role();
            role.setLibelle("user");
            nouvelUtilisateur.setRole(Role.find("libelle",role.getLibelle()).singleResult());

            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] encryptedPassword = md.digest(utilisateur.getPassword().getBytes());
                nouvelUtilisateur.setPassword(Base64.getEncoder().encodeToString(encryptedPassword));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            if (utilisateur.getRole() != null) {
                nouvelUtilisateur.setRole(Role.findById(utilisateur.getRole().getId()));
            }

            nouvelUtilisateur.persist();
            return nouvelUtilisateur;
        } else {
            return null;
        }
    }

    @Transactional
    public Utilisateur getUtilisateur(Utilisateur user) {
        String userPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] encryptedPassword = md.digest(user.getPassword().getBytes());
            userPassword = Base64.getEncoder().encodeToString(encryptedPassword);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Utilisateur oldUser = Utilisateur.find("mail",user.getMail()).singleResult();
        if (oldUser.getPassword().equals(userPassword)) {
            return oldUser;
        } else {
            return null;
        }
    }

    @Transactional
    public Utilisateur updateUtilisateur(Utilisateur user) {
        Utilisateur updateUser = Utilisateur.find("mail",user.getMail()).singleResult();
        updateUser.setMail(user.getMail());
        updateUser.setNom(user.getNom());
        updateUser.setPrenom(user.getPrenom());
        updateUser.setRole(Role.find("id",user.getRole().getId()).singleResult());
        updateUser.persist();
        return updateUser;
    }

    @Transactional
    public void deleteUtilisateur(Utilisateur utilisateur){
        Utilisateur userToDelete = Utilisateur.find("mail",utilisateur.getMail()).singleResult();
        if(userToDelete.isPersistent()){
            userToDelete.delete();
        }
    }

}

package fr.epsi.services;

import fr.epsi.models.Fournisseur;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;

@Singleton
public class FournisseurService {

    public List<Fournisseur> getAllFournisseur() {
        //Méthode permettant de récupérer tous les fournisseurs dans la base de données fournit par panacheEntity
        return Fournisseur.findAll().list();
    }

    @Transactional
    public Fournisseur addFournisseur(Fournisseur fournisseur) {
        if (Fournisseur.find("siret", fournisseur.getSiret()).count() == 0) {
            Fournisseur newFournisseur = new Fournisseur(fournisseur.getDenomination(), fournisseur.getSiret(), fournisseur.getAdresse(), fournisseur.getNomGerant(), fournisseur.getPrenomGerant());

            //Méthode permettant de sauvegarder directement un fournisseur dans la base de données fournit par panacheEntity
            newFournisseur.persist();
            return newFournisseur;
        } else {
            return null;
        }
    }

    @Transactional
    public Fournisseur updateFournisseur(Fournisseur fournisseur){
        Fournisseur fournisseurToUpdate = Fournisseur.find("siret",fournisseur.getSiret()).singleResult();
        fournisseurToUpdate.setAdresse(fournisseur.getAdresse());
        fournisseurToUpdate.setDenomination(fournisseur.getDenomination());
        fournisseurToUpdate.setNomGerant(fournisseur.getNomGerant());
        fournisseurToUpdate.setPrenomGerant(fournisseur.getPrenomGerant());
        fournisseurToUpdate.persist();
        return fournisseurToUpdate;
    }

    @Transactional
    public void deleteFournisseur(Fournisseur fournisseur){
        Fournisseur fournisseurToDelete = Fournisseur.find("siret",fournisseur.getSiret()).singleResult();
        if(fournisseurToDelete.isPersistent()){
            fournisseurToDelete.delete();
        }
    }

}

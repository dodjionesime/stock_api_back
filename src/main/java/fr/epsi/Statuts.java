package fr.epsi;

public enum Statuts {
    CREATED("Crée"), ARCHIVED("Archivé"), DELIVERED("Livraison");

    private String status;

    private Statuts(String statut) {
        this.status = statut;
    }

    public String getStatus() {
        return status;
    }

}

INSERT INTO `categorie` VALUES (1,'Pull'),(2,'Tshirt'),(3,'Pantalon'),(4,'Chaussettes'),(5,'Veste');

INSERT INTO `fournisseur` VALUES (1,'54087 Dexter Parkway','Cohen & Steers Infrastructure Fund, Inc','Dowzell','Kenyon','19249088475638'),(2,'63944 Duke Point','Verisk Analytics, Inc.','Spalding','Tiffie','17545129841625'),(3,'818 Eliot Court','Valvoline Inc.','Corpe','Odilia','56225551188407'),(4,'165 Everett Place','MDC Partners Inc.','Laxtonne','Jeramey','40152430490021'),(5,'1 Manley Avenue','Green Dot Corporation','Andrioni','Vittoria','54604484726202'),(6,'3 Esker Drive','Mobileye N.V.','Hancorn','Fonz','33301454979309'),(7,'9 Fuller Circle','CoreCivic, Inc.','Hellis','Dimitri','45962966175232'),(8,'01580 Di Loreto Place','Verona Pharma plc','Froom','Phillie','74740901706520'),(9,'7 Forest Run Junction','Capital Senior Living Corporation','Moran','Margret','01134506404769');

INSERT INTO `produit` VALUES (1,'Progressive homogeneous encoding','Wine - Chianti Classica Docg',15,'CREATED',4,7),
(2,'Object-based content-based pricing structure','Oysters - Smoked',0,'ARCHIVED',5,7),
(3,'Horizontal client-server leverage','Corn - Cream, Canned',25,'DELIVERED',4,7),
(4,'Open-source multi-state analyzer','Scallops - In Shell',2,'CREATED',5,4),
(5,'Progressive fault-tolerant info-mediaries','Sandwich Wrap',10,'CREATED',3,8),
(6,'Cloned uniform leverage','Ham - Cooked Italian',33,'CREATED',2,8),
(7,'Horizontal upward-trending pricing structure','Buffalo - Striploin',4,'CREATED',5,9),
(8,'Horizontal disintermediate software','Cactus Pads',22,'CREATED',5,9),
(9,'Multi-tiered multimedia encryption','Muffin Batt - Ban Dream Zero',10,'CREATED',4,9),
(10,'Open-source homogeneous superstructure','Syrup - Monin - Granny Smith',2,'CREATED',1,9),
(11,'Balanced regional software','Syrup - Pancake',8,'CREATED',5,9);

INSERT INTO `imageproduit` VALUES (1,'https://images.asos-media.com/products/asos-design-ultimate-parka-kaki/11674963-1-khaki',3),(2,'https://images.asos-media.com/products/pimkie-duffle-coat-a-capuche-bordee-de-fausse-fourrure-kaki/14147630-1-green',7),(3,'https://images.asos-media.com/products/asos-design-piper-chaussures-a-talon-mi-haut-avec-lien-a-nouer-a-la-jambe-noir-croco/13615031-1-blackcroc',6),(4,'https://images.asos-media.com/products/nike-plus-sweat-shirt-court-a-petit-logo-virgule-avec-cordon-elastique-gris/13257424-1-silverlilac',2),(5,'https://images.asos-media.com/products/asos-design-t-shirt-col-v-a-manches-longues-gris-chine/12451541-1-grey',4),(6,'https://images.asos-media.com/products/asos-design-top-en-tulle-a-imprime-papillon-effet-photo/14389619-1-multi',6),(7,'https://images.asos-media.com/products/vans-t-shirt-a-petit-logo-gris/12224472-1-grey',7),(8,'https://images.asos-media.com/products/reebok-classics-t-shirt-avec-imprime-reflechissant-dans-le-dos-blanc-exclusivite-asos/13923770-1-white',5),(9,'https://images.asos-media.com/products/asos-design-valentines-t-shirt-oversize-avec-imprime-cur-sur-lensemble/14095411-1-shadedspruce',9),(10,'https://images.asos-media.com/products/collusion-t-shirt-a-manches-longues-blanc/13382964-1-white',6);

INSERT INTO `mouvement` VALUES (1,'9999-12-31 23:59:59.000000','CREATED', 1),
(2,'9999-12-31 23:59:59.000000', 'ARCHIVED', 2),
(3,'9999-12-31 23:59:59.000000', 'DELIVERED', 3);

INSERT INTO `role` VALUES (1,'admin'),(2,'user');

INSERT INTO `utilisateur` VALUES (1,'sullivan.delaby@epsi.fr','Sullivan','4QrcOUm6Wau+VuBX8g+IPg==','Delaby',1),(2,'dodji.onesime@epsi.fr','Dodji','4QrcOUm6Wau+VuBX8g+IPg==','Onesime',1),(3,'jeremy.thery@epsi.fr','Jeremy','4QrcOUm6Wau+VuBX8g+IPg==','Thery',2),(4,'justine.moreau@epsi.fr','Justine','4QrcOUm6Wau+VuBX8g+IPg==','Moreau',2);

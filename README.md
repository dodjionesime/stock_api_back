Créer la base de données **stock** dans phpmyadmin

Dans le fichier application.properties 
modifier le username et le password de votre base de données pour permettre la connexion à celle-ci.
 **Ne créer aucune table dans la base de données** car le processus hibernate paramétré le fait déjà
 
 Pour compiler le  projet 
 **mvnw compile quarkus:dev -Dquarkus.http.port=8081** si votre port 8080 est déjà utilisé par un autre service.
 
 Dans votre navigateur après avoir démarré le backend, tapez : **http://localhost:8081/swagger-ui/**
 vous aurez afficher la liste de tous les appels post et get actuellement implémenté et qui doivent être appelé dans le front.
 Vous pourriez donc faire des tests et vérifier dans votre base de données si les appels get et post fonctionnent






 